let rec split cmp x a i j =
  assert (i <= j);
  if i = j then
    (* The segment has width 0. *)
    i
  else
    (* The segment has width at least 1. *)
    (* We have a pivot element at index [k] and two subintervals [i, k)
       and [k+1, j), excluding the pivot. *)
    let k = (i + j) / 2 in
    assert (i <= k && k < j);
    if cmp x a.(k) <= 0 then
      (* [x <= a.(k)] holds. Narrow the search to the left subinterval. *)
      split cmp x a i k
    else
      (* [a.(k) < x] holds. Narrow the search to the right subinterval. *)
      split cmp x a (k+1) j
