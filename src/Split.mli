(**[split cmp x a i j] performs a binary search for the value [x] in the
   sorted array segment delimited by the indices [i] and [j] in array [a].
   The inequality [i <= j] must hold, and the indices [i] and [j] delimit
   the semi-open interval [i, j).

   [split] returns an index [k] such that [i <= k <= j] holds and every
   element below index [k] is less than [x] and every element at or above
   index [k] is greater than or equal to [x].

   Thus, the array segment [i, j) is split into two consecutive segments
   [i, k) and [k, j).

   This is a sequential algorithm. Its time complexity is {i O(n)}, where
   {i n} is the length of the input segment, that is, [j - i]. *)
val split :
  (* order: *)
  ('a -> 'a -> int) ->
  (* pivot: *)
  'a ->
  (* input segment: *)
  'a array -> int -> int ->
  (* splitting index: *)
  int
