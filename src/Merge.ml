(* This wrapper defines [merge] in terms of [merge_segments]. *)

let[@inline] merge merge_segments cmp a1 a2 =
  let n1 = Array.length a1
  and n2 = Array.length a2 in
  if n1 = 0 then
    Array.copy a2
  else if n2 = 0 then
    Array.copy a1
  else
    let dummy = a1.(0) in
    let n = n1 + n2 in
    let a = Array.make n dummy in
    merge_segments cmp a1 0 n1 a2 0 n2 a 0 n;
    a

module Sequential = struct

  (* This sequential version of [merge_segments] runs in linear time. *)

  let rec merge_segments cmp a1 i1 j1 a2 i2 j2 a i j =
    assert (i1 <= j1 && i2 <= j2 && i <= j);
    assert (j1 - i1 + j2 - i2 = j - i);
    if i1 = j1 then
      (* The first input segment is empty. *)
      Array.blit a2 i2 a i (j - i)
    else if i2 = j2 then
      (* The second input segment is empty. *)
      Array.blit a1 i1 a i (j - i)
    else
      (* Both segments are nonempty. *)
      let x1 = a1.(i1)
      and x2 = a2.(i2) in
      if cmp x1 x2 <= 0 then begin
        (* x1 <= x2 *)
        a.(i) <- x1;
        merge_segments cmp a1 (i1 + 1) j1 a2 i2 j2 a (i + 1) j
      end
      else begin
        (* x2 < x1 *)
        a.(i) <- x2;
        merge_segments cmp a1 i1 j1 a2 (i2 + 1) j2 a (i + 1) j
      end

  let merge cmp a1 a2 =
    merge merge_segments cmp a1 a2

end

module Parallel = struct

  (* This parallel version of [merge_segments] has work O(n) and span
     O(log^2 n). *)

  let rec merge_segments cmp a1 i1 j1 a2 i2 j2 a i j =
    assert (i1 <= j1 && i2 <= j2 && i <= j);
    assert (j1 - i1 + j2 - i2 = j - i);
    let n1, n2, n = j1 - i1, j2 - i2, j - i in
    if n <= 16 (* TODO granularity control? *) then
      (* A small task is treated in sequential mode. *)
      Sequential.merge_segments cmp a1 i1 j1 a2 i2 j2 a i j
    else if n2 < n1 then
      (* The first segment is longer. *)
      merge_segments_aux cmp a1 i1 j1 n1 a2 i2 j2 n2 a i j
    else
      (* The second segment is longer. *)
      merge_segments_aux cmp a2 i2 j2 n2 a1 i1 j1 n1 a i j

  and merge_segments_aux cmp a1 i1 j1 n1 a2 i2 j2 n2 a i j =
    assert (j1 - i1 = n1 && j2 - i2 = n2);
    (* We assume that the first segment is longer. *)
    assert (n1 >= n2);
    (* We assume that the first segment is nonempty. *)
    assert (n1 > 0);
    (* Compute the midpoint of the first segment. *)
    let k1 = (i1 + j1) / 2 in
    assert (i1 <= k1 && k1 < j1);
    (* Use the element [x1] found at index [k1] in the first segment
       as a pivot. Split the second segment using this pivot. *)
    let x1 = a1.(k1) in
    let k2 = Split.split cmp x1 a2 i2 j2 in
    assert (i2 <= k2 && k2 <= j2);
    (* TODO run these calls in parallel *)
    (* Merge the two left-hand segments. *)
    let k = i + (k1 - i1) + (k2 - i2) in
    merge_segments cmp a1 i1 k1 a2 i2 k2 a i k;
    (* Copy the pivot. *)
    a.(k) <- x1;
    (* Merge the two right-hand segments. *)
    merge_segments cmp a1 (k1 + 1) j1 a2 k2 j2 a (k + 1) j

  let merge cmp a1 a2 =
    merge merge_segments cmp a1 a2

end
