module Sequential : sig

  val merge_segments:
    (* order: *)
    ('a -> 'a -> int) ->
    (* first source segment: *)
    'a array -> int -> int ->
    (* second source segment: *)
    'a array -> int -> int ->
    (* destination segment: *)
    'a array -> int -> int ->
    (* no result *)
    unit

  val merge:
    (* order: *)
    ('a -> 'a -> int) ->
    (* first source array: *)
    'a array ->
    (* second source array: *)
    'a array ->
    (* fresh result array: *)
    'a array

end

module Parallel : sig

  val merge_segments:
    (* order: *)
    ('a -> 'a -> int) ->
    (* first source segment: *)
    'a array -> int -> int ->
    (* second source segment: *)
    'a array -> int -> int ->
    (* destination segment: *)
    'a array -> int -> int ->
    (* no result *)
    unit

  val merge:
    (* order: *)
    ('a -> 'a -> int) ->
    (* first source array: *)
    'a array ->
    (* second source array: *)
    'a array ->
    (* fresh result array: *)
    'a array

end
