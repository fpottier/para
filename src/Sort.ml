module Sequential = struct

  (* [merge_segments] does not operate in place; it requires disjoint source
     and destination areas. This implies that we cannot implement a function
     [sort_segment] that operates in place. This suggests writing a function
     [sort_and_move_segment] that uses disjoint source and destination areas.
     However, giving a recursive formulation of [sort_and_move_segment] is not
     easy either. A way out of this problem is to 1- allow [sort_segment] to
     use a scratch area, and 2- make [sort_segment] and
     [sort_and_move_segment] mutually recursive. *)

  (* [sort_segment cmp s i j t] sorts the segment [i, j) of the array [s] in
     place, and uses the segment [i, j) of the array [t] as a scratch area. *)

  let rec sort_segment cmp s i j t =
    assert (0 <= i && i <= j && j <= Array.length s);
    assert (Array.length s = Array.length t);
    let n = j - i in
    if n <= 1 then
      (* The source segment has width 0 or 1. *)
      ()
    else if n = 2 then begin
      (* The source segment has width 2. *)
      let x1 = s.(i)
      and x2 = s.(i + 1) in
      if cmp x1 x2 > 0 then (s.(i) <- x2; s.(i + 1) <- x1)
    end
    else begin
      (* The source segment has width at least 2. *)
      (* Split it in two (nonempty) subsegments. *)
      let k = (i + j) / 2 in
      assert (i < k && k < j);
      (* Sort the subsegments, placing the sorted data into the array [t]. *)
      sort_segment_and_move cmp s i k t;
      sort_segment_and_move cmp s k j t;
      (* Merge the sorted subsegments, moving the data back into [s]. *)
      Merge.Sequential.merge_segments cmp t i k t k j s i j
    end

  (* [sort_segment_and_move cmp s i j t] sorts the segment [i, j) of the array
     [s] and writes the sorted data into the segment [i, j) of the array [t]. *)

  and sort_segment_and_move cmp s i j t =
    assert (0 <= i && i <= j && j <= Array.length s);
    assert (Array.length s = Array.length t);
    let n = j - i in
    if n = 0 then
      (* The source segment is empty. *)
      ()
    else if n = 1 then
      (* The source segment has width 1. *)
      t.(i) <- s.(i)
    else if n = 2 then begin
      (* The source segment has width 2. *)
      let x1 = s.(i)
      and x2 = s.(i + 1) in
      if cmp x1 x2 <= 0 then (t.(i) <- x1; t.(i + 1) <- x2)
      else (t.(i) <- x2; t.(i + 1) <- x1)
    end
    else begin
      (* Split the source segment in two (nonempty) subsegments. *)
      let k = (i + j) / 2 in
      assert (i < k && k < j);
      (* Sort the subsegments in place, using [t] as a scratch area. *)
      sort_segment cmp s i k t;
      sort_segment cmp s k j t;
      (* Merge the sorted subsegments, moving the data into [t]. *)
      Merge.Sequential.merge_segments cmp s i k s k j t i j
    end

  (* [shadow s] returns a fresh array of the same length as the array [s],
     containing arbitrary data. *)

  let shadow s =
    let n = Array.length s in
    if n = 0 then s else Array.make n s.(0)

  (* [sort_and_move cmp s t] sorts the array [s] and writes the sorted data into
     the array [t]. *)

  let sort_and_move cmp s t =
    assert (Array.length s = Array.length t);
    let n = Array.length s in
    sort_segment_and_move cmp s 0 n t

  (* [sort cmp s] sorts the array [s] in place. *)

  let sort cmp s =
    sort_and_move cmp s (shadow s)

end
