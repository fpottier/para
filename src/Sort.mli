module Sequential : sig

  (**[sort_and_move cmp s t] writes a sorted copy of the array [s] into the
     array [t]. *)
  val sort_and_move:
    (* order: *)
    ('a -> 'a -> int) ->
    (* source array: *)
    'a array ->
    (* destination array: *)
    'a array ->
    (* no result *)
    unit

  (**[sort cmp s] sorts the array [s] in place. *)
  val sort:
    (* order: *)
    ('a -> 'a -> int) ->
    (* array: *)
    'a array ->
    (* no result *)
    unit

end
