To compile this test, type `make`.

To run this test, type `make random` or `make test`.
The test runs until it is interrupted.

This test requires the package `monolith`.
