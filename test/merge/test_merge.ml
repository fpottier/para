open Monolith

(* Specifications for sorted lists, arrays, sorted arrays. *)

let sorted spec =
  let sort xs = List.sort Int.compare xs in
  let neg =
    map_outof
      sort
      (sort, constant "List.sort Int.compare")
      spec
  and pos =
    spec
  in
  ifpol neg pos

let array spec =
  let neg =
    map_outof
      Array.of_list
      (Array.of_list, constant "Array.of_list")
      spec
  and pos =
    map_into
      Array.to_list
      (Array.to_list, constant "Array.to_list")
      spec
  in
  ifpol neg pos

(* Reference. *)

module R = struct

  let merge cmp a1 a2 =
    List.merge cmp (Array.to_list a1) (Array.to_list a2)
    |> Array.of_list

end

(* A bound on elements. *)

let element =
  lt 64

let length =
  Gen.lt (8 * 1024)

let array =
  array (sorted (list ~length element))

(* Declare the operations. *)

let () =

  let spec = array ^> array ^> array in
  declare "Para.Merge.Sequential.merge Int.compare" spec
    (R.merge Int.compare)
    (Para.Merge.Sequential.merge Int.compare);

  let spec = array ^> array ^> array in
  declare "Para.Merge.Parallel.merge Int.compare" spec
    (R.merge Int.compare)
    (Para.Merge.Parallel.merge Int.compare);

  ()

(* Start the engine! *)

let () =
  let fuel = 1 in
  main fuel
