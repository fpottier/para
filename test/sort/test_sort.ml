open Monolith

(* Specifications for arrays. *)

let array spec =
  let neg =
    map_outof
      Array.of_list
      (Array.of_list, constant "Array.of_list")
      spec
  and pos =
    map_into
      Array.to_list
      (Array.to_list, constant "Array.to_list")
      spec
  in
  ifpol neg pos

(* A bound on elements. *)

let element =
  lt 64

let length =
  Gen.lt (8 * 1024)

let array =
  array (list ~length element)

(* Declare the operations. *)

let () =

  let spec = array ^> unit in
  declare "Para.Sort.Sequential.sort Int.compare" spec
    (Array.sort Int.compare)
    (Para.Sort.Sequential.sort Int.compare);

  ()

(* Start the engine! *)

let () =
  let fuel = 1 in
  main fuel
